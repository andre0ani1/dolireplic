# Copyright (C) 2022 SuperAdmin
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#
# Generic
#

ModuleDoliReplicName = DoliReplic
ModuleDoliReplicDesc = Dolibarr replication module
ReplicDone = Replication done
DoliReplicArea = DoliReplic
DoliReplicAreaInfo = DoliReplic - Informations
DoliReplicAreaReplic = DoliReplic - Replication
previousBackup = Previous backup
NoBackupFileAvailable = No file
loginBDD = User login
passBDD = User password
LOGO_TEST = Put a test logo
LOGO_TESTTooltip = Put a test logo on the test Dolibarr
loginSaved = Informations saved
ChemVide = The path to the production Dolibarr is empty
FichVide = The configuration file of the production Dolibarr is not found
ErrBDDCour = Backup error on the test Dolibarr
ErrBDDDest = Backup error on the test Dolibarr
SaveBDDCour = Backup of the test DB done
DesactMail = Deactivation of emails
DesactNotif = Deactivation of notifications
OptionLogo = Copy of the test logo
OptionCustom = Copy of the custom directory
ErreurOptionCustom = Error while Copying the custom directory
OptionMail = Replacement of email addresses
OptionMycomp = Copy of the mycompany directory
OptionDoc = Copy of the document directory
ErreurSupprBDDDest = Deletion error of the test DB
ErreurCreaBDDDest = Error creating the test DB
PasDeBackup = There is no backup on the Dolibarr to replicate
CopieLogo = Copy test logo
RemplacementMailSoc = Repacement of the customer emails
RemplacementMailSocp = Replacement of the contacts emails
RemplacementMailAdh = Replacement of the member emails
RemplacementMailUser = Replacement of users emails
ErreurOptionMailSoc = Error while repacement of the customer emails
ErreurOptionMailSocP = Error while repacement of the contacts emails
ErreurOptionMailAdh = Error while repacement of the members emails
ErreurOptionMailUser = Error while repacement of the users emails
DesactNotif = Deactivation of notifications
MauvaisNomBackup = The backup name is not found
nomBackup = The backup name is empty
cheminBackupProd = The path of the backup of production is empty
ErreurCopieFichier=Error while copiyng the backup file
OkCopieFichier=Backup file succeffully copied
FichierNonTrouve=The backup file is not found
ErrorBddColorEldy=Error while changing color of Eldy theme
ErrorBddColorOblyonTop=Error while changing color of top menu Oblyon theme
ErrorBddColorOblyonLeft=Error while changing color of left menu Oblyon theme
BddColorOblyonTop=Change color of Oblyon in top menu
BddColorOblyonLeft=Change color of Oblyon on left menu
BddColorEldy=Change color of Eldy theme
ErrorDesactMails=Error while deactivating emails
DesactMails=Deactivating emails
SyslogFacility=Change log level
ErrorNettoyageBDD=Error while changing name in database
OkNettoyageBDD=Cleaning database
ErrorDicModuleDesact=Error while saving modules to deactivate

#
# Admin page
#
DoliReplicSetup = Configuration of DoliReplic module
Settings = Configuration
DoliReplicSetupPage = Configuration of DoliReplic module
DOLIREPLIC_OPT_DOC = Replication of documents directory
DOLIREPLIC_OPT_DOCTooltip = Do we replic documents directory
DOLIREPLIC_OPT_DOC_COMP = Do we replic documents mycompany
DOLIREPLIC_OPT_COMPTooltip = Do we replic documents mycompany
DOLIREPLIC_OPT_CUST = Replication of custom directory
DOLIREPLIC_OPT_CUSTTooltip = Do we replic custom directory
DOLIREPLIC_OPT_LOGO = Put a test logo
DOLIREPLIC_OPT_LOGOTooltip = Replace logo by a test logo
DOLIREPLIC_PROD = Path to the production Dolibarr
DOLIREPLIC_PRODTooltip = Put the path on the server without / at the end
DOLIREPLIC_PROD_DOC = Path to the documents on the production Dolibarr
DOLIREPLIC_PROD_DOCTooltip = Put the path on the server without / at the end
DOLIREPLIC_REPLACE_MAIL = Replace emails
DOLIREPLIC_REPLACE_MAILTooltip = Do we replace the mails of users, customers, contacts
DOLIREPLIC_BACKUP_DIR = Path to the backups on the production 
DOLIREPLIC_BACKUP_DIRTooltip = Put the path of the backup files on the production
DOLIREPLIC_BACKUP_NAME = Name of the backup file
DOLIREPLIC_BACKUP_NAMETooltip = Fill the name of the backup file
DOLIREPLIC_BDD_NAME_PROD=Name of the production BDD
DOLIREPLIC_BDD_NAME_PRODTooltip=Put the name of the production database

#
# Dictionnaire
#
ModuleDesactive=Module to deactivate
Module_name=Class name of the module

#
# About page
#
About = About
DoliReplicAbout = About DoliReplic
DoliReplicAboutPage = About DoliReplic

#
# Tache planifiee
#
PlanificationReplication = Dolibarr replication