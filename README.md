# Module DOLIREPLIC pour [DOLIBARR ERP CRM](https://www.dolibarr.org)

## Fonctionnalités

Le module DoliReplic permet de répliquer une instance de Dolibarr sur une autre (prod vers test).
Copie possible de la base de données, des documents, des modules externes, changement de logo. Possibilité de remplacer les adresses mails et de désactiver certains modules.
Affiche des informations sur le serveur et les modules externes.


## Utilisation

- Une fois le module activé, il faut se rendre sur la page de configuration pour définir le chemin du Dolibarr à répliquer et choisir les options disponibles. Vous pouvez aussi, via le dictionnaire "Modules à désactiver", choisir certains modules à désactiver.
- On se rend ensuite dans le menu Outils, il y a un sous-menu Dolireplic avec Infos et Réplication. Dans Infos, vous aurez des informations sur votre serveur et votre Dolibarr. Et Réplication permet de lancer la réplication.


## Cas d'usage

Vous avez un Dolibarr de production et un Dolibarr de test pour travailler. Sur celui de production, vous avez vos données réelles de vos clients.
DoliReplic permet de répliquer les données de votre Dolibarr de production sur celui de test afin de garder vos deux instances les plus identiques possibles lorsque vous devez travailler sur celui de test, pour avoir des données à jour.
Au lieu de devoir répliquer manuellement les données et documents, la BDD, la configuration, etc... Il suffit d'un clic pour avoir vos deux instances identitques.


## Licenses

### Code

GPLv3

### Documentation

Texte et documentation sous GFDL.


### Source 

Toutes les sources sont disponibles sur le [Gitlab du projet](https://gitlab.com/andre0ani1/dolireplic).



![Configuration](./img/captures/Dolireplic_Configuration.png)

![Informations](./img/captures/DoliReplic_Informations.png)

![Configuration](./img/captures/DoliReplic_R%C3%A9plication.png)

