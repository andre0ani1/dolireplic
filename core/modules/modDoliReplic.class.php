<?php
/* Copyright (C) 2004-2018  Laurent Destailleur     <eldy@users.sourceforge.net>
 * Copyright (C) 2018-2019  Nicolas ZABOURI         <info@inovea-conseil.com>
 * Copyright (C) 2019-2020  Frédéric France         <frederic.france@netlogic.fr>
 * Copyright (C) 2022 SuperAdmin
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * 	\defgroup   dolireplic     Module DoliReplic
 *  \brief      DoliReplic module descriptor.
 *
 *  \file       htdocs/dolireplic/core/modules/modDoliReplic.class.php
 *  \ingroup    dolireplic
 *  \brief      Description and activation file for module DoliReplic
 */
include_once DOL_DOCUMENT_ROOT . '/core/modules/DolibarrModules.class.php';

/**
 *  Description and activation class for module DoliReplic
 */
class modDoliReplic extends DolibarrModules
{
	/**
	 * Constructor. Define names, constants, directories, boxes, permissions
	 *
	 * @param DoliDB $db Database handler
	 */
	public function __construct($db)
	{
		global $langs, $conf;
		$this->db = $db;

		$this->numero = 700000;
		$this->rights_class = 'dolireplic';
		$this->family = "AALL";
		$this->module_position = '30';
		$this->name = preg_replace('/^mod/i', '', get_class($this));
		$this->description = "DoliReplicDescription";
		$this->descriptionlong = "DoliReplicDescription";
		$this->editor_name = 'Patrice Andreani';
		$this->editor_email = 'andreani.patrice@net-c.fr';
		$this->editor_url = 'https://gitlab.com/andre0ani1/dolireplic';
		$this->version = '5.0';
		$this->const_name = 'MAIN_MODULE_' . strtoupper($this->name);
		$this->picto = 'replic@dolireplic';

		// Define some features supported by module (triggers, login, substitutions, menus, css, etc...)
		$this->module_parts = array(
			'triggers' => 0,
			'login' => 0,
			'substitutions' => 0,
			'menus' => 0,
			'tpl' => 0,
			'barcode' => 0,
			'models' => 0,
			'printing' => 0,
			'theme' => 0,
			'css' => array(
				'dolireplic/css/fichier.css',
			),
			'js' => array(),
			'hooks' => array(),
			'moduleforexternal' => 0,
		);

		// Data directories to create when module is enabled.
		// Example: this->dirs = array("/dolireplic/temp","/dolireplic/subdir");
		$this->dirs = array("/dolireplic/temp");

		// Config pages. Put here list of php page, stored into dolireplic/admin directory, to use to setup module.
		$this->config_page_url = array("setup.php@dolireplic");

		// Dependencies
		// A condition to hide module
		$this->hidden = false;

		$this->depends = array();
		$this->requiredby = array();
		$this->conflictwith = array();

		// The language file dedicated to your module
		$this->langfiles = array("dolireplic@dolireplic");

		// Prerequisites
		$this->phpmin = array(7, 0); // Minimum version of PHP required by module
		$this->need_dolibarr_version = array(14, 0); // Minimum version of Dolibarr required by module

		// Messages at activation
		$this->warnings_activation = array(); // Warning to show when we activate module. array('always'='text') or array('FR'='textfr','MX'='textmx'...)
		$this->warnings_activation_ext = array(); // Warning to show when we activate an external module. array('always'='text') or array
		//$this->const = array(1 => array('DOLIREPLIC_DEST', 'chaine', '', 'chemin du dolibarr a repliquer', 1, 'allentities', 1));

		if (!isset($conf->dolireplic) || !isset($conf->dolireplic->enabled)) {
			$conf->dolireplic = new stdClass();
			$conf->dolireplic->enabled = 0;
		}

		// Array to add new pages in new tabs
		$this->tabs = array();

		// Dictionaries
		$this->dictionaries=array(
			'langs'=>'dolireplic@dolireplic',
			'tabname'=>array(MAIN_DB_PREFIX."dolireplic_mod_desact"),
			'tablib'=>array("ModuleDesactive"),	
			'tabsql'=>array('SELECT md.rowid as rowid, md.module_name, md.label, md.active FROM '.MAIN_DB_PREFIX.'dolireplic_mod_desact as md'),
			'tabsqlsort'=>array("module_name ASC"),
			'tabfield'=>array("module_name,label"),
			'tabfieldvalue'=>array("module_name,label"),
			'tabfieldinsert'=>array("module_name,label"),
			'tabrowid'=>array("rowid"),	
			'tabcond'=>array($conf->dolireplic->enabled)
		);


		// Boxes/Widgets
		// Add here list of php file(s) stored in dolireplic/core/boxes that contains a class to show a widget.
		$this->boxes = array();

		// Cronjobs (List of cron jobs entries to add when module is enabled)
		// unit_frequency must be 60 for minute, 3600 for hour, 86400 for day, 604800 for week
		$this->cronjobs = array();


		// Permissions provided by this module
		$this->rights = array();
		$r = 0;
		// Add here entries to declare new permissions
		/* BEGIN MODULEBUILDER PERMISSIONS */
		$this->rights[$r][0] = $this->numero . sprintf("%02d", $r + 1);
		$this->rights[$r][1] = 'Lire les données de DoliReplic';
		$this->rights[$r][4] = 'dolireplic';
		$this->rights[$r][5] = 'read';
		$r++;
		$this->rights[$r][0] = $this->numero . sprintf("%02d", $r + 1);
		$this->rights[$r][1] = 'Modifier les données de DoliReplic';
		$this->rights[$r][4] = 'dolireplic';
		$this->rights[$r][5] = 'write';
		$r++;
		$this->rights[$r][0] = $this->numero . sprintf("%02d", $r + 1);
		$this->rights[$r][1] = 'Supprimer les données de DoliReplic';
		$this->rights[$r][4] = 'dolireplic';
		$this->rights[$r][5] = 'delete';
		$r++;
		/* END MODULEBUILDER PERMISSIONS */

		$this->menu = array();
		$r = 0;
		/* BEGIN MODULEBUILDER TOPMENU */
		$this->menu[$r++] = array(
			'fk_menu' => 'fk_mainmenu=tools',
			'type' => 'left',
			'titre' => 'Dolireplic',
			'prefix' => img_picto('', $this->picto, 'class="paddingright pictofixedwidth em092"'),
			'mainmenu' => 'tools',
			'leftmenu' => 'dolireplic',
			'langs' => 'dolireplic@dolireplic',
			'position' => 1108 + $r,
			'enabled' => '$conf->dolireplic->enabled',
			'perms'=>'$user->rights->dolireplic->dolireplic->read && $user->rights->dolireplic->dolireplic->write && $user->rights->dolireplic->dolireplic->delete',
			'target' => '',
			'user' => 2
		);

		$this->menu[$r++] = array(
			'fk_menu' => 'fk_mainmenu=tools,fk_leftmenu=dolireplic',
			'type' => 'left',
			'titre' => 'Infos',
			'mainmenu' => 'tools',
			'url' => '/dolireplic/dolireplicinfo.php',
			'langs' => 'dolireplic@dolireplic',
			'position' => 1109 + $r,
			'enabled' => '$conf->dolireplic->enabled',
			'perms'=>'$user->rights->dolireplic->dolireplic->read',
			'target' => '',
			'user' => 2
		);

		$this->menu[$r++] = array(
			'fk_menu' => 'fk_mainmenu=tools,fk_leftmenu=dolireplic',
			'type' => 'left',
			'titre' => 'Réplication',
			'mainmenu' => 'tools',
			'url' => '/dolireplic/dolireplicreplic.php',
			'langs' => 'dolireplic@dolireplic',
			'position' => 1110 + $r,
			'enabled' => '$conf->dolireplic->enabled',
			'perms'=>'$user->rights->dolireplic->dolireplic->read && $user->rights->dolireplic->dolireplic->write && $user->rights->dolireplic->dolireplic->delete',
			'target' => '',
			'user' => 2
		);



		/* END MODULEBUILDER TOPMENU */

		// Exports profiles provided by this module
		$r = 1;

		// Imports profiles provided by this module
		$r = 1;
	}

	/**
	 *  Function called when module is enabled.
	 *  The init function add constants, boxes, permissions and menus (defined in constructor) into Dolibarr database.
	 *  It also creates data directories
	 *
	 *  @param      string  $options    Options when enabling module ('', 'noboxes')
	 *  @return     int             	1 if OK, 0 if KO
	 */
	public function init($options = '')
	{
		global $conf, $langs;

		//$result = $this->_load_tables('/install/mysql/tables/', 'dolireplic');
		$result = $this->_load_tables('/dolireplic/sql/');
		if ($result < 0) {
			return -1; // Do not activate module if error 'not allowed' returned when loading module SQL queries (the _load_table run sql with run_sql with the error allowed parameter set to 'default')
		}


		// Permissions
		$this->remove($options);

		$sql = array();

		// Document templates
		$moduledir = dol_sanitizeFileName('dolireplic');
		$myTmpObjects = array();
		$myTmpObjects['MyObject'] = array('includerefgeneration' => 0, 'includedocgeneration' => 0);

		foreach ($myTmpObjects as $myTmpObjectKey => $myTmpObjectArray) {
			if ($myTmpObjectKey == 'MyObject') {
				continue;
			}
			if ($myTmpObjectArray['includerefgeneration']) {
				$src = DOL_DOCUMENT_ROOT . '/install/doctemplates/' . $moduledir . '/template_myobjects.odt';
				$dirodt = DOL_DATA_ROOT . '/doctemplates/' . $moduledir;
				$dest = $dirodt . '/template_myobjects.odt';

				if (file_exists($src) && !file_exists($dest)) {
					require_once DOL_DOCUMENT_ROOT . '/core/lib/files.lib.php';
					dol_mkdir($dirodt);
					$result = dol_copy($src, $dest, 0, 0);
					if ($result < 0) {
						$langs->load("errors");
						$this->error = $langs->trans('ErrorFailToCopyFile', $src, $dest);
						return 0;
					}
				}

				$sql = array_merge($sql, array(
					"DELETE FROM " . MAIN_DB_PREFIX . "document_model WHERE nom = 'standard_" . strtolower($myTmpObjectKey) . "' AND type = '" . $this->db->escape(strtolower($myTmpObjectKey)) . "' AND entity = " . ((int) $conf->entity),
					"INSERT INTO " . MAIN_DB_PREFIX . "document_model (nom, type, entity) VALUES('standard_" . strtolower($myTmpObjectKey) . "', '" . $this->db->escape(strtolower($myTmpObjectKey)) . "', " . ((int) $conf->entity) . ")",
					"DELETE FROM " . MAIN_DB_PREFIX . "document_model WHERE nom = 'generic_" . strtolower($myTmpObjectKey) . "_odt' AND type = '" . $this->db->escape(strtolower($myTmpObjectKey)) . "' AND entity = " . ((int) $conf->entity),
					"INSERT INTO " . MAIN_DB_PREFIX . "document_model (nom, type, entity) VALUES('generic_" . strtolower($myTmpObjectKey) . "_odt', '" . $this->db->escape(strtolower($myTmpObjectKey)) . "', " . ((int) $conf->entity) . ")"
				));
			}
		}

		return $this->_init($sql, $options);
	}

	/**
	 *  Function called when module is disabled.
	 *  Remove from database constants, boxes and permissions from Dolibarr database.
	 *  Data directories are not deleted
	 *
	 *  @param      string	$options    Options when enabling module ('', 'noboxes')
	 *  @return     int                 1 if OK, 0 if KO
	 */
	public function remove($options = '')
	{
		$sql = array();
		return $this->_remove($sql, $options);
	}
}
