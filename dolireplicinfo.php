<?php
/* Copyright (C) 2001-2005 Rodolphe Quiedeville <rodolphe@quiedeville.org>
 * Copyright (C) 2004-2015 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2005-2012 Regis Houssin        <regis.houssin@inodbox.com>
 * Copyright (C) 2015      Jean-François Ferry	<jfefe@aternatik.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *	\file       dolireplic/dolireplicindex.php
 *	\ingroup    dolireplic
 *	\brief      Home page of dolireplic top menu
 */



// Load Dolibarr environment
$res = 0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (!$res && !empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) {
	$res = @include $_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/main.inc.php";
}
// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp = empty($_SERVER['SCRIPT_FILENAME']) ? '' : $_SERVER['SCRIPT_FILENAME'];
$tmp2 = realpath(__FILE__);
$i = strlen($tmp) - 1;
$j = strlen($tmp2) - 1;
while ($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i] == $tmp2[$j]) {
	$i--;
	$j--;
}
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1)) . "/main.inc.php")) {
	$res = @include substr($tmp, 0, ($i + 1)) . "/main.inc.php";
}
if (!$res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php")) {
	$res = @include dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php";
}
// Try main.inc.php using relative path
if (!$res && file_exists("../main.inc.php")) {
	$res = @include "../main.inc.php";
}
if (!$res && file_exists("../../main.inc.php")) {
	$res = @include "../../main.inc.php";
}
if (!$res && file_exists("../../../main.inc.php")) {
	$res = @include "../../../main.inc.php";
}
if (!$res) {
	die("Include of main fails");
}

require_once DOL_DOCUMENT_ROOT . '/core/lib/admin.lib.php';
require_once DOL_DOCUMENT_ROOT . '/core/lib/files.lib.php';
require_once DOL_DOCUMENT_ROOT . '/core/class/utils.class.php';
require_once DOL_DOCUMENT_ROOT . '/core/class/html.formfile.class.php';
require_once DOL_DOCUMENT_ROOT . '/core/lib/security2.lib.php';
include_once DOL_DOCUMENT_ROOT . '/core/lib/functions.lib.php';
include_once DOL_DOCUMENT_ROOT . '/core/lib/security.lib.php';

// Load translation files required by the page
$langs->loadLangs(array("dolireplic@dolireplic"));

$action = GETPOST('action', 'aZ09');


// Security check
/* if (!$user->rights->dolireplic->objreplic->read) {
	accessforbidden();
} */
$socid = GETPOST('socid', 'int');
if (isset($user->socid) && $user->socid > 0) {
	$action = '';
	$socid = $user->socid;
}


/*
 * Actions
 */


/*
 * View
 */

$form = new Form($db);
$formfile = new FormFile($db);
$outputfile = $conf->admin->dir_output . '/dolireplic/output/';

llxHeader("", $langs->trans("DoliReplicAreaInfo"));

print load_fiche_titre($langs->trans("DoliReplicAreaInfo"), '', 'objreplic@dolireplic');

if (!empty($conf->dolireplic->enabled) && ($user->rights->dolireplic->dolireplic->read)) {


	//
	// informations syteme et modules
	//

	print '<div class = "centpercent" style = "width: 80%; margin: auto; line-height: 1.4">';
	print '<div class="titre">';
	print '<h2><span class="fa fa-info-circle"></span> Informations système</h2><br>';
	print '</div>';
	print '<strong>Serveur :</strong> ' . $_SERVER['SERVER_SOFTWARE'] . '<br>';
	print '<strong>IP du serveur :</strong> ' . $_SERVER['SERVER_ADDR'] . '<br>';
	print '<strong>Version de PHP :</strong> ' . phpversion() . '<br>';
	$output = shell_exec('mysql -V');
	preg_match('@[0-9]+\.[0-9]+\.[0-9]+@', $output, $version);
	print '<strong>Base de données :</strong> ' . $dolibarr_main_db_type . ' ' . $version[0] . '<br>';

	if (function_exists('opcache_get_status') && opcache_get_status()['opcache_enabled']) {
		print '<strong>OpCache :</strong> actif<br>';
	} else {
		print '<strong>OpCache :</strong> inactif<br>';
	}

	$bytes2 = disk_total_space($dolibarr_main_document_root);
	$si_prefix2 = array('B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB');
	$base2 = 1024;
	$class2 = min((int)log($bytes2, $base2), count($si_prefix2) - 1);
	echo sprintf('<strong>Espace disque total :</strong>  %1.2f', $bytes2 / pow($base2, $class2)) . ' ' . $si_prefix2[$class2] . '<br>';

	$bytes = disk_free_space($dolibarr_main_document_root);
	$si_prefix = array('B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB');
	$base = 1024;
	$class = min((int)log($bytes, $base), count($si_prefix) - 1);
	echo sprintf('<strong>Espace disque libre :</strong> %1.2f', $bytes / pow($base, $class)) . ' ' . $si_prefix[$class] . '<br>';

	$test = $dolibarr_main_document_root;
	$prod = $conf->global->DOLIREPLIC_PROD . '/htdocs';

	$espaceTest = shell_exec('du ' . $test . ' -h --max-depth=0');
	echo '<strong>Espace occupé sur dolitest :</strong> ' . $espaceTest . '<br>';
	$espaceProd = shell_exec('du ' . $prod . ' -h --max-depth=0');
	echo '<strong>Espace occupé sur doliprod :</strong> ' . $espaceProd . '<br>';
	print '<br>';
	print '<hr>';
	print '</div>';


	//
	// infos dolitest
	//

	$dirTest = $dolibarr_main_document_root;
	$cmd = 'cd ' . $dirTest;
	$cmd = 'cd ..' . ' && git status';
	$statusTest = shell_exec($cmd);
	$cmd = 'cd ' . $dirTest . ' && git branch';
	$branchTest = shell_exec($cmd);
	$cmd = 'cd ' . $dirTest . ' && git show -s';
	$commitTest = shell_exec($cmd);
	$arrTest = explode("\n", $commitTest);
	$numc = $arrTest[0];
	$dateDoliTest = $arrTest[3];

	$prefix = "commit";
	$index = strpos($numc, $prefix) + strlen($prefix);
	$commitTest2 = substr($numc, $index);

	$prefix = "Date:";
	$index = strpos($dateDoliTest, $prefix) + strlen($prefix);
	$dateDoliTest2 = substr($dateDoliTest, $index);

	$donedir[$dir] = new stdClass();
	$donedir[$dir]->status = $statusTest;
	$donedir[$dir]->branch = $branchTest;


	//
	// infos doliprod
	//

	$dirProd = $conf->global->DOLIREPLIC_PROD;
	$cmd = 'cd ' . $dirProd;
	$cmd = 'cd ..' . ' && git status';
	$statusProd = shell_exec($cmd);
	$cmd = 'cd ' . $dirProd . ' && git branch';
	$branchProd = shell_exec($cmd);
	$cmd = 'cd ' . $dirProd . ' && git show -s';
	$commit = shell_exec($cmd);
	$arrProd = explode("\n", $commit);
	$numc = $arrProd[0];
	$dateDoliProd = $arrProd[3];

	$prefix = "commit";
	$index = strpos($numc, $prefix) + strlen($prefix);
	$commitProd2 = substr($numc, $index);

	$prefix = "Date:";
	$index = strpos($dateDoliProd, $prefix) + strlen($prefix);
	$dateDoliProd2 = substr($dateDoliProd, $index);

	$donedir[$dir] = new stdClass();
	$donedir[$dir]->status = $statusProd;
	$donedir[$dir]->branch = $branchProd;



	//
	// affichage infos
	//


	print '<div class = "centpercent" style = "width: 80%; margin: auto">';
	print '<table style = "margin: auto; width: 100%">';
	print '<tr>';
	print '  <th><h2><font color="green"><span class="fa fa-server"></span> Dolitest</font></h2></th>';
	print '  <th><h2><font color="#ff8333"><span class="fa fa-server"></span> Doliprod</font></h2></th>';
	print '</tr>';

	print '<tr>';
	print '<td style = "padding: 5px;"> <strong>Version de Dolibarr</strong> : ' . DOL_VERSION . '</td>';
	print '<td style = "padding: 5px;"> <strong>Version de Dolibarr</strong> : ' . DOL_VERSION . '</td>';
	print '</tr>';

	print '<tr>';
	print '<td style = "padding: 5px;"> <strong>Branche</strong> : ' . $branchTest . '</td>';
	print '<td style = "padding: 5px;"> <strong>Branche</strong> : ' . $branchProd . '</td>';
	print '</tr>';

	print '<tr>';
	print '<td style = "padding: 5px;"> <strong>Dernier commit</strong> : ' . $commitTest2 . '</td>';
	print '<td style = "padding: 5px;"> <strong>Dernier commit</strong> : ' . $commitProd2 . '</td>';
	print '</tr>';

	print '<tr>';
	print '<td style = "padding: 5px;"> <strong>Date dernier commit</strong> : ' . $dateDoliTest2 . '</td>';
	print '<td style = "padding: 5px;"> <strong>Date dernier commit</strong> : ' . $dateDoliProd2 . '</td>';
	print '</tr>';

	print '<tr>';
	print '<td style = "padding: 5px;"> <strong>Chemin de Dolibarr</strong> : ' . $dirTest . '</td>';
	print '<td style = "padding: 5px;"> <strong>Chemin de Dolibarr</strong> : ' . $dirProd . '</td>';
	print '</tr>';

	print '<tr>';
	print '<td style = "padding: 5px;"> <strong>Statut Git</strong> : <div class="statutgit">' . $statusTest . '</div></td>';
	print '<td style = "padding: 5px;"> <strong>Statut Git</strong> : <div class="statutgit">' . $statusProd . '</div></td>';
	print '</tr>';

	print '<tr>';
	print '<td>';
	
	print '</td>';
	print '</tr>';

	print '</table> ';

	print '<br><br>';


	print '<button class="b2 button button-save">Statut Git (montrer)</button>';
	print '<button class="b1 button button-save">Statut Git (cacher)</button>';

	print '<script type="text/javascript" language="javascript">';
	print 'jQuery(document).ready(function () {  
	$(".b1").click(function(){
	$(".statutgit").hide();
	});
	$(".b2").click(function(){
	$(".statutgit").show();
	});
	});';
	print '</script>';

	print '<br>';
	print '<hr>';
	print '<br>';

	print '</div>';

	


	//
	// infos modules dolitest
	//


	$filename = array();
	$modulesTest = array();
	$i = 0;
	$j = 0;
	$modNameLoaded = array();
	$customTest = $dolibarr_main_document_root_alt . '/';

	foreach (glob($customTest . '*', GLOB_ONLYDIR) as $dir) {
		dol_syslog("Scan directory " . $dir . " for module descriptor files (modXXX.class.php)");
		$handle = @opendir($dir . '/core/modules/');
		if (is_resource($handle)) {
			while (($file = readdir($handle)) !== false) {
				if (is_readable($dir . '/core/modules/' . $file) && substr($file, 0, 3) == 'mod' && substr($file, dol_strlen($file) - 10) == '.class.php') {
					$modName = substr($file, 0, dol_strlen($file) - 10);
					if ($modName) {
						try {
							$dirname = basename($dir);
							$res = include_once $dir . '/core/modules/' . $file;
							if (class_exists($modName)) {
								try {
									$objMod = new $modName($db);
									$modNameLoaded[$modName] = $dir;
									$external = ($objMod->isCoreOrExternalModule() == 'external');
									if ($external) {
										if ($dirname != 'dolireplic') {

											$modulesTest[$modName] = [
												'name' => $objMod->getName(),
												'publisher' => $objMod->getPublisher(),
												'version' => $objMod->getVersion(0),
												'enabled' => $conf->$dirname->enabled,
											];
										}
									}
									$j++;
									$i++;
								} catch (Exception $e) {
									dol_syslog("Failed to load " . $dir . $file . " " . $e->getMessage(), LOG_ERR);
								}
							} else {
								print "Warning bad descriptor file : " . $dir . $file . " (Class " . $modName . " not found into file)<br>";
							}
						} catch (Exception $e) {
							dol_syslog("Failed to load " . $dir . $file . " " . $e->getMessage(), LOG_ERR);
						}
					}

					if ($dirname != 'dolireplic') {
						$cmd = 'cd ' . $dir . ' && git show -s --format=%H';
						$modulesTest[$modName]['commit'] = shell_exec($cmd);

						$cmd = 'cd ' . $dir . ' && git branch';
						$modulesTest[$modName]['branch'] = shell_exec($cmd);

						$cmd = 'cd ' . $dir . ' && git status';
						$modulesTest[$modName]['status'] = shell_exec($cmd);

						$cmd = 'cd ' . $dir . ' && git show -s --format=%ci';
						$modulesTest[$modName]['date'] = shell_exec($cmd);
					}
				}
			}
			closedir($handle);
		} else {
			dol_syslog("htdocs/admin/modulehelp.php: Failed to open directory " . $dir . ". See permission and open_basedir option.", LOG_WARNING);
		}
	}


	//
	// infos modules doliprod
	//


	$filename = array();
	$modulesProd = array();
	$inc1 = 0;
	$inc2 = 0;
	$modNameLoaded = array();
	$customProd = $prod . '/custom';

	foreach (glob($customProd . '/' . '*', GLOB_ONLYDIR) as $dir) {
		dol_syslog("Scan directory " . $dir . " for module descriptor files (modXXX.class.php)");
		$handle = @opendir($dir . '/core/modules/');
		$dirname = basename($dir);
		if (is_resource($handle)) {
			while (($file = readdir($handle)) !== false) {
				if (is_readable($dir . '/core/modules/' . $file) && substr($file, 0, 3) == 'mod' && substr($file, dol_strlen($file) - 10) == '.class.php') {
					$modName = substr($file, 0, dol_strlen($file) - 10);
					if ($modName) {
						try {
							$res = include_once $dir . $file;
							// $res = include_once $dir . '/core/modules/' . $file;
							if (class_exists($modName)) {
								try {
									$objMod = new $modName($db);
									$modNameLoaded[$modName] = $dir;
									$external = ($objMod->isCoreOrExternalModule() == 'external');
									if ($external) {
										$versionF = $dir . '/VERSION';
										if (file_exists($versionF)) {
											$f = fopen($versionF, 'r');
											$versionEM = fgets($f);
											fclose($f);
										}

										$fichModProd = $dir . '/core/modules/' . $file;
										$param_list = array();
										$pattern = '/^([^\'"]*)(([\'"]).*\3)(.*)$/';
										$search_list = array(
											'editeurP' => "this->editor_name",
											'versionP' => "this->version = '",
										);

										$file = fopen($fichModProd, "r");
										$nb_to_found = count($search_list);
										while ($line = fgets($file)) {
											foreach ($search_list as $key => $value) {
												if (strpos($line, $value) !== false) {
													if (preg_match($pattern, $line, $matches)) {
														$param_list[$key] = $matches[2];
													}
												}
											}
											if (count($param_list) >= $nb_to_found) {
												break;
											}
										}
										if ($file) {
											fclose($file);
										}

										$editeurP = str_replace("'", "", $param_list['editeurP']);
										$versionP = str_replace("'", "", $param_list['versionP']);
										$modulesProd[$modName] = [
											'enabled' => $conf->$dirname->enabled,
											'editeurP' => $editeurP,
											'versionP' => $versionP,
											'nameP' => $dirname,
											'versionEM' => $versionEM,
										];
									}
									$inc1++;
									$inc2++;
									$versionEM = '';
								} catch (Exception $e) {
									dol_syslog("Failed to load " . $dir . $file . " " . $e->getMessage(), LOG_ERR);
								}
							} else {
								print "Warning bad descriptor file : " . $dir . $file . " (Class " . $modName . " not found into file)<br>";
							}
						} catch (Exception $e) {
							dol_syslog("Failed to load " . $dir . $file . " " . $e->getMessage(), LOG_ERR);
						}
					}

					$cmd = 'cd ' . $dir . ' && git show -s --format=%H';
					$modulesProd[$modName]['commit'] = shell_exec($cmd);

					$cmd = 'cd ' . $dir . ' && git branch';
					$modulesProd[$modName]['branch'] = shell_exec($cmd);

					$cmd = 'cd ' . $dir . ' && git status';
					$modulesProd[$modName]['status'] = shell_exec($cmd);

					$cmd = 'cd ' . $dir . ' && git show -s --format=%ci';
					$modulesProd[$modName]['date'] = shell_exec($cmd);
				}
			}
			closedir($handle);
		} else {
			dol_syslog("htdocs/admin/modulehelp.php: Failed to open directory " . $dir . ". See permission and open_basedir option.", LOG_WARNING);
		}
	}

	print '<div class = "centpercent" style = "width: 80%; margin: auto; font-size: 17px; line-height: 1.4;">';
	print '<h2><span class="fa fa-info-circle"></span> Etat des modules par rapport à l\'instance de production</h2><br>';

	foreach (array_unique(array_merge(array_keys($modulesTest), array_keys($modulesProd))) as $modName) {

		if ($modulesTest[$modName]['date'] > $modulesProd[$modName]['date']) {
			print $modulesTest[$modName]['name'] . '...............<i class="fas fa-arrow-up" style="color: #ff8333;"></i><br>';
		}

		if ($modulesTest[$modName]['date'] < $modulesProd[$modName]['date']) {
			print $modulesTest[$modName]['name'] . '...............<i class="fas fa-arrow-down" style="color: green;"></i><br>';
		}

		if ($modulesTest[$modName]['date'] == $modulesProd[$modName]['date']) {
			print $modulesTest[$modName]['name'] . '...............<i class="fas fa-equals" style="color: #77767b;"></i><br>';
		}
	}

	print '<br>';
	print '<hr>';

	print '</div>';


	print '<div class = "centpercent" style = "width: 80%; margin: auto; font-size: 15px; line-height: 1.4;">';

	print '<h2><span class="fa fa-info-circle"></span> Informations sur les modules externes</h2><br>';
   
	print '<div style="overflow-x:auto;">';
	print '<table style = "border: 2px solid #898989; margin: auto; table-layout: auto; width: 100%; word-break: break-all;">';

	print '<thead>';
	print '<tr style = "background-color: #91b4ff;"> <td style = "border: 2px solid  #898989; padding: 4px; width: 20%"></td> <td style = "border: 2px solid  #898989; padding: 4px; width: 40%"><h2><font color="green">Dolitest</font></h2></td> <td style = "border: 2px solid  #898989; padding: 4px; width: 40%"><h2><font color="#ff8333">Doliprod</font></h2></td> </tr>';
	print '</thead>';

	foreach (array_unique(array_merge(array_keys($modulesTest), array_keys($modulesProd))) as $modName) {

		$isInTest = array_key_exists($modName, $modulesTest);
		$isInProd = array_key_exists($modName, $modulesProd);
		if ($isInTest || $isInProd) {

			print '<tbody>';

			print '<tr style = "background-color: #ffb9ab"> <td class=datacellone; style = "border: 2px solid #898989; padding: 7px;"><strong>Nom du module</strong></td> <td style = "border: 2px solid  #898989; padding: 7px;"><strong>' . $modulesTest[$modName]['name'];
			if ($modulesTest[$modName]['enabled'] > 0) {
				print ' <span class="badge  badge-dot badge-status4 classfortooltip badge-status" title="Activé" aria-label="Activé"></span></strong></td>';
			} else {
				print ' <span class="badge  badge-dot badge-status1 classfortooltip badge-status" title="Désactivé" aria-label="Désactivé"></span></strong></td>';
			};

			print '<td style = "border: 2px solid #898989; padding: 7px;"><strong>' . $modulesProd[$modName]['nameP'];
			if ($modulesProd[$modName]['enabled'] > 0) {
				print ' <span class="badge  badge-dot badge-status4 classfortooltip badge-status" title="Activé" aria-label="Activé"></span></strong></td>';
			} else {
				print ' <span class="badge  badge-dot badge-status1 classfortooltip badge-status" title="Désactivé" aria-label="Désactivé"></span></strong></td>';
			};

			print '<tr> <td style = "border: 2px solid  #898989; padding: 7px;"><strong>Editeur</strong></td> <td style = "border: 2px solid  #898989; padding: 7px;">' . $modulesTest[$modName]['publisher'] . '</td> <td style = "border: 2px solid  #898989; padding: 7px;">' . $modulesProd[$modName]['editeurP'] . '</td> </tr>';

			print '<tr style = "background-color: #dedede"> <td style = "border: 2px solid  #898989; padding: 7px;"><strong>Version</strong></td> <td style = "border: 2px solid  #898989; padding: 7px;">' . $modulesTest[$modName]['version'] . '</td> <td style = "border: 2px solid  #898989; padding: 7px;">';
			if ($modulesProd[$modName]['versionEM'] != null) print $modulesProd[$modName]['versionEM'];
			else print $modulesProd[$modName]['versionP'] . '</td> </tr>';

			print '<tr> <td style = "border: 2px solid  #898989; padding: 7px;"><strong>Branche Git</strong></td> <td style = "border: 2px solid  #898989; padding: 7px;">' . $modulesTest[$modName]['branch'] . '</td> <td style = "border: 2px solid  #898989; padding: 7px;">' . $modulesProd[$modName]['branch'] . '</td> </tr>';

			print '<tr style = "background-color: #dedede"> <td style = "border: 2px solid  #898989; padding: 7px;"><strong>Numéro du dernier commit</strong></td> <td style = "border: 2px solid  #898989; padding: 7px;">' . $modulesTest[$modName]['commit'] . '</td> <td style = "border: 2px solid  #898989; padding: 7px;">' . $modulesProd[$modName]['commit'] . '</td> </tr>';

			print '<tr> <td style = "border: 2px solid  #898989; padding: 7px;"><strong>Date du dernier commit</strong></td> <td style = "border: 2px solid  #898989; padding: 7px;">' . $modulesTest[$modName]['date'] . '</td> <td style = "border: 2px solid  #898989; padding: 7px;">' . $modulesProd[$modName]['date'] . '</td> </tr>';

			print '<tr style = "background-color: #dedede"> <td style = "border: 2px solid  #898989; padding: 7px;"><strong>Statut Git</strong></td> <td style = "border: 2px solid  #898989; padding: 7px;"><div class="statutgit">' . $modulesTest[$modName]['status'] . '</div></td> <td style = "border: 2px solid  #898989; padding: 7px;"><div class="statutgit">' . $modulesProd[$modName]['status'] . '</div></td></tr>';
			print '</tbody>';
		}
	}
	print '</table>';
	print '</div>';
	print '</div>';

	print '<br>';
} else {
	accessforbidden();
}

// End of page
llxFooter();
$db->close();
