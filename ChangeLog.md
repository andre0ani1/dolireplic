# CHANGELOG du module DOLIREPLIC pour [DOLIBARR ERP CRM](https://www.dolibarr.org)

### 0.1
 - Première version  


### 1.0
 - Réplication de la base de données et des documents  


### 1.2
 - Liste des fichiers de sauvegarde  
 - Traduction en anglais  


### 2.0
 - Options pour documents et logo  
 - Changement menu  
 - Infos de connexion BDD  
 - Désactivation des mails  


### 2.2
 - Moins de configuration
 - Chemin du Dolibarr de destination
 - Récupération des infos depuis conf.php
 - Déchiffrement mdp (Spé Easya)


### 2.3
 - Ajout option copier custom
 - Infos sur les modules externes


### 3.6
 - Installation sur l'instance de test
 - Recuperation de la BDD depuis le repertoire de backup pour la prod
 - Preparation PHP 8


### 4.1
 - Possibilité de répliquer depuis un serveur distant


### 4.2
 - Test sur PHP 7.0.33, PHP 7.4, PHP 8.1


### 4.3
- Modifie le niveau de syslogd
- Modifie les couleurs d'oblyon
- Possibilité de désactiver des modules


### 4.4
- Remplacement des mails 


### 4.5
 - Amélioration du code
 

### 4.6
 - Nouvelles options pour choisir le repertoire des sauvegardes et le nom du fichier


### 4.7
 - Ajout d'informations sur les differences de version des modules externes entre la prod et la test


### 4.8
 - Correction récupération informations modules prod


### 4.9
 - Diverses corrections (copie custom...)


### 5.0
- correction copie backup, ajout dans lang en_US