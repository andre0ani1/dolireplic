<?php
/* Copyright (C) 2001-2005 Rodolphe Quiedeville <rodolphe@quiedeville.org>
 * Copyright (C) 2004-2015 Laurent Destailleur  <eldy@users.sourceforge.net>
 * Copyright (C) 2005-2012 Regis Houssin        <regis.houssin@inodbox.com>
 * Copyright (C) 2015      Jean-François Ferry	<jfefe@aternatik.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *	\file       dolireplic/dolireplicindex.php
 *	\ingroup    dolireplic
 *	\brief      Home page of dolireplic top menu
 */



// Load Dolibarr environment
$res = 0;

if (!defined('CSRFCHECK_WITH_TOKEN')) {
	define('CSRFCHECK_WITH_TOKEN', '1');		// Force use of CSRF protection with tokens even for GET
}

// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if (!$res && !empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) {
	$res = @include $_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/main.inc.php";
}
// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp = empty($_SERVER['SCRIPT_FILENAME']) ? '' : $_SERVER['SCRIPT_FILENAME'];
$tmp2 = realpath(__FILE__);
$i = strlen($tmp) - 1;
$j = strlen($tmp2) - 1;
while ($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i] == $tmp2[$j]) {
	$i--;
	$j--;
}
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1)) . "/main.inc.php")) {
	$res = @include substr($tmp, 0, ($i + 1)) . "/main.inc.php";
}
if (!$res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php")) {
	$res = @include dirname(substr($tmp, 0, ($i + 1))) . "/main.inc.php";
}
// Try main.inc.php using relative path
if (!$res && file_exists("../main.inc.php")) {
	$res = @include "../main.inc.php";
}
if (!$res && file_exists("../../main.inc.php")) {
	$res = @include "../../main.inc.php";
}
if (!$res && file_exists("../../../main.inc.php")) {
	$res = @include "../../../main.inc.php";
}
if (!$res) {
	die("Include of main fails");
}

require_once DOL_DOCUMENT_ROOT . '/core/lib/admin.lib.php';
require_once DOL_DOCUMENT_ROOT . '/core/lib/files.lib.php';
require_once DOL_DOCUMENT_ROOT . '/core/class/utils.class.php';
require_once DOL_DOCUMENT_ROOT . '/core/class/html.formfile.class.php';
require_once DOL_DOCUMENT_ROOT . '/core/lib/security2.lib.php';
include_once DOL_DOCUMENT_ROOT . '/core/lib/functions.lib.php';
include_once DOL_DOCUMENT_ROOT . '/core/lib/security.lib.php';

include_once DOL_DOCUMENT_ROOT . '/custom/dolireplic/class/dolireplic.class.php';
// include_once DOL_DOCUMENT_ROOT . '/custom/dolireplic/class/dolireplicProcess.class.php';
// include 'processus.php';
// include_once DOL_DOCUMENT_ROOT . '/custom/dolireplic/processus.php';


// Load translation files required by the page
$langs->loadLangs(array("dolireplic@dolireplic", "admin"));

$action = GETPOST('action', 'aZ09');


// Security check
/* if (!$user->rights->dolireplic->objreplic->read) {
	accessforbidden();
} */
$socid = GETPOST('socid', 'int');
if (isset($user->socid) && $user->socid > 0) {
	$action = '';
	$socid = $user->socid;
}


/*
 * Actions
 */

global $db;

$db = getDoliDBInstance($conf->db->type, $conf->db->host, $dolibarr_main_db_user, $dolibarr_main_db_pass, $dolibarr_main_db_name, $conf->db->port);

// suppression des fichiers de sauvegarde

// pour dolibarr <= 14
if ($action == 'delete') {

	$back2 = $conf->admin->dir_output . '/dolireplic/backup/' . basename(GETPOST('urlfile', 'alpha'));
	$ret = dol_delete_file($back2, 1, 1, 0, 0, 1);
	if (empty($ret)) {
		setEventMessages($langs->trans("ErrorFailToDeleteFile", GETPOST('urlfile')), null, 'errors');
	} else {
		setEventMessages($langs->trans("FileWasRemoved", GETPOST('urlfile')), null, 'mesgs');
	}
	$action = '';
}

// pour dolibarr > 14
if ($action == 'deletefile') {
	$back2 = $conf->admin->dir_output . '/dolireplic/backup/' . basename(GETPOST('urlfile', 'alpha'));
	$ret = dol_delete_file($back2, 1, 1, 0, 0, 1);
	if (empty($ret)) {
		setEventMessages($langs->trans("ErrorFailToDeleteFile", GETPOST('urlfile')), null, 'errors');
	} else {
		setEventMessages($langs->trans("FileWasRemoved", GETPOST('urlfile')), null, 'mesgs');
	}
	$action = '';
}




// debut replication
// preparation des dossiers
if ($action == 'startReplic') {

	if(empty($conf->global->DOLIREPLIC_BACKUP_NAME)) {
		setEventMessages($langs->trans("nomBackup"), null, 'errors');
		header('Location: ' . $_SERVER['PHP_SELF']);
		exit();
	}

	if(empty($conf->global->DOLIREPLIC_BACKUP_DIR)) {
		setEventMessages($langs->trans("cheminBackupProd"), null, 'errors');
		header('Location: ' . $_SERVER['PHP_SELF']);
		exit();
	}

	$fichier = $conf->global->DOLIREPLIC_PROD . '/htdocs/conf/conf.php';
	$repBack = $conf->admin->dir_output . '/dolireplic/backup';
	$repWork = $conf->admin->dir_output . '/dolireplic/work';

	if (!file_exists($repBack)) {
		dol_mkdir($repBack);
	}

	if (!file_exists($repWork)) {
		dol_mkdir($repWork);
	}

	$utils = new Utils($db);
	$outputfile = $conf->admin->dir_output . '/dolireplic/output/';
	if (!file_exists($outputfile)) {
		dol_mkdir($outputfile);
	}


	if (empty($conf->global->DOLIREPLIC_PROD)) {
		setEventMessages($langs->trans("ChemVide"), null, 'errors');
	} else {
		if (!file_exists($fichier)) {
			setEventMessages($langs->trans("FichVide"), null, 'errors');
		} else {
			// recup et lis les infos depuis le conf.php de l'instance de test
			/* $param_list = array();
			$pattern = '/^([^\'"]*)(([\'"]).*\3)(.*)$/';
			$search_list = array(
				'user' => "user='",
				'pass' => "pass='",
				'db_name' => "db_name='",
				'data_root' => "data_root='",
				'document_root' => "document_root='"
			); */

			/* $file = fopen($fichConfTest, "r");
			$nb_to_found = count($search_list);
			while ($line = fgets($file)) {
				foreach ($search_list as $key => $value) {
					if (strpos($line, $value) !== false) {
						if (preg_match($pattern, $line, $matches)) {
							$param_list[$key] = $matches[2];
						}
					}
				}
				if (count($param_list) >= $nb_to_found) {
					break;
				}
			}
			if ($file) {
				fclose($file);
			}


			$passbdd = str_replace("'", "", $param_list['pass']);
			$userbdd = str_replace("'", "", $param_list['user']);
			$pathdoc = str_replace("'", "", $param_list['data_root']);
			$namebdd = str_replace("'", "", $param_list['db_name']);
			$pathhtdocs = str_replace("'", "", $param_list['document_root']); */


			// dechiffrement (si chiffre) mdp dans conf.php
			/* if ((!empty($passbdd) && preg_match('/crypted:/i', $passbdd)) || !empty($passbddenc)) {
				if (!empty($passbdd) && preg_match('/crypted:/i', $passbdd)) {
					$passbdd = preg_replace('/crypted:/i', '', $passbdd);
					$passbdd = dol_decode($passbdd);
					$passbddenc = $passbdd;
				} else {
					$passbdd = dol_decode($passbddenc);
				}
			} */


			$ficTest = $dolibarr_main_db_name . '.sql' . '';

			// chemin des backups sur la prod
			$bddProd = $conf->global->DOLIREPLIC_BACKUP_DIR;

			// nom du fichier de backup
			$backupName = $conf->global->DOLIREPLIC_BACKUP_NAME;

			if (dol_dir_is_emtpy($bddProd)) {
				setEventMessages($langs->trans("PasDeBackup"), null, 'errors');
				header('Location: ' . $_SERVER['PHP_SELF']);
				exit();
			}

			$file7 = $repWork . '/locBdd/';
			dol_delete_dir_recursive($file7);
			if (!file_exists($file7)) {
				dol_mkdir($file7);
			}
			$cop1 = 'cp -Rap ' . $bddProd . '/.' . ' ' . $file7;
			// TODO a remplacer par une fonction specifique
			$result = $utils->executeCLI($cop1, $outputfile);
			

			// recuperation du fichier de backup de la prod le plus recent et contenant le bon nom de fichier
			function dol_most_recent_file_with_name($dir, $name) {
				$files = glob($dir . '/*' . $name . '*');
				$recentFile = null;
				$recentTime = 0;
				
				foreach ($files as $file) {
					if (is_file($file) && filemtime($file) > $recentTime) {
						$recentTime = filemtime($file);
						$recentFile = $file;
					}
				}
				return $recentFile;
			}

			
			$ficRecent = dol_most_recent_file_with_name($file7, $backupName);
			if ($ficRecent !== null) {
				$ficProd = basename($ficRecent);
				$file6 = $repWork . '/PROD-' . $ficProd;
				
				if (!copy($ficRecent, $file6)) {
					setEventMessages($langs->trans("ErreurCopieFichier"), null, 'errors');
					header('Location: ' . $_SERVER['PHP_SELF']);
					exit();
				} else {
					setEventMessages($langs->trans("OkCopieFichier"), null, 'success');
				}
			} else {
				setEventMessages($langs->trans("FichierNonTrouve"), null, 'errors');
				header('Location: ' . $_SERVER['PHP_SELF']);
				exit();
			}
			


			/* $bddProd4 = dol_most_recent_file($file7);
			$chemFile4 = $bddProd4['fullname'];
			$ficProd = dol_basename($chemFile4);
			if (strpos($ficProd, $backupName) !== false) {
				$file6 = $repWork . '/PROD-' . $ficProd;
			} else {
				setEventMessages($langs->trans("MauvaisNomBackup"), null, 'errors');
				header('Location: ' . $_SERVER['PHP_SELF']);
				exit();
			}
			dol_copy($chemFile4, $file6, 0, 1); */
			


			// chemin des fichiers
			$repTestBackDoliProd = $repBack . '/' . $ficTest;
			$repTestBackDoliTest = $repBack . '/' . $ficProd;
			$repTestWorkDoliTest = $repWork . '/' . $ficTest;
			$repTestWorkDoliProd = $repWork . '/' . $ficProd;

			// $file6 = $repWork . '/PROD-' . $ficProd;
			$monFichier = $file6['fullname'];
			// copy de la bdd de prod et gunzip
			dol_copy($monFichier, $repTestWorkDoliProd, 0, 1);
			dol_copy($monFichier, $repTestBackDoliTest, 0, 1);
			
			$gunzip1 = 'gunzip ' . $file6;
			// TODO a remplacer par une fonction specifique
			$result = $utils->executeCLI($gunzip1, $outputfile);


			// dump de la bdd de la test
			// TODO a remplacer par une fonction specifique
			$dump1 = shell_exec('MYSQL_PWD="' . $dolibarr_main_db_pass . '" mysqldump -u ' . $dolibarr_main_db_user . ' ' . $dolibarr_main_db_name . ' > ' . $repWork . '/' . $ficTest . '');
			if (!empty($dump1)) {
				setEventMessages($langs->trans("ErrBDDCour"), null, 'errors');
				exit();
			}
			setEventMessages($langs->trans("SaveBDDCour"), null, 'mesgs');


			// sauvegarde des fichiers originaux
			$date = date('dmy-hi');
			dol_copy($repTestWorkDoliTest, $repTestBackDoliProd, 0, 1);
			dol_copy($repTestWorkDoliProd, $repTestBackDoliTest, 0, 1);
			$origin2 = $repBack . '/' . $date . '-' . $ficTest;
			$arrive2 = $repBack . '/' . $date . '-' . $ficProd;
			dol_move($repTestBackDoliProd, $origin2, 0, 1, 0, 1);
			dol_move($repTestBackDoliTest, $arrive2, 0, 1, 0, 1);
			$compress2 = dol_compress_file($origin2, $origin2 . '.gz', 'gz');
			if ($compress2 > 0) {
				dol_delete_file($origin2);
			}

			// suppression des anciennes sauvegardes
			// 2 semaines en secondes : 1209600
			// 1 mois en secondes : 2629800
			$backdir = $conf->admin->dir_output . '/dolireplic/backup/';
			foreach (glob($backdir . "*") as $file1) {
				if (time() - filectime($file1) > 1209600) {
					unlink($file1);
				}
			}

			// recuperation des informations dans le dictionnaire pour desactiver certains modules
			$moduleList = array();
			$sql = "SELECT module_name, label FROM " . MAIN_DB_PREFIX . "dolireplic_mod_desact";
			$sql .= " WHERE module_name IS NOT NULL";
			$result = $db->query($sql);
			if ($result) {
				while ($obj = $db->fetch_object($resql)) {
					$moduleList[] = $obj;
				}
			}
			$db->free();


			// pour la sauvegarde des options dolireplic dans la bbd
			$optionDOC = $conf->global->DOLIREPLIC_OPT_DOC;
			$optionCOMP = $conf->global->DOLIREPLIC_OPT_COMP;
			$optionCUST = $conf->global->DOLIREPLIC_OPT_CUST;
			$optionLOGO = $conf->global->DOLIREPLIC_OPT_LOGO;
			$prod = $conf->global->DOLIREPLIC_PROD;
			$chemDocProd = $conf->global->DOLIREPLIC_PROD_DOC;
			$actif = $conf->global->MAIN_MODULE_DOLIREPLIC;
			$optionMAIL = $conf->global->DOLIREPLIC_REPLACE_MAIL;
			$optionDirBackup = $conf->global->DOLIREPLIC_BACKUP_NAME;
			$optionNameBackup = $conf->global->DOLIREPLIC_BACKUP_NAME;
			$nomBDDProd = $conf->global->DOLIREPLIC_BDD_NAME_PROD;


			// suppression, creation bdd
			// TODO a remplacer par une fonction specifique
			$drop = shell_exec('MYSQL_PWD="' . $dolibarr_main_db_pass . '" mysql -u ' . $dolibarr_main_db_user . ' -e "drop database ' . $dolibarr_main_db_name . '"');
			if (!empty($drop)) {
				setEventMessages($langs->trans("ErreurSupprBDDDest"), null, 'errors');
				exit();
			}

			$createbdd = $db->DDLCreateDb($dolibarr_main_db_name, $dolibarr_main_db_character_set, $dolibarr_main_db_collation, $dolibarr_main_db_user);
			if (!$createbdd) {
				setEventMessages($langs->trans("ErrorCreateBDD"), null, 'errors');
			}

			$fileBddProd = basename($file6, '.gz');
			$repWork2 = $repWork . '/' . $fileBddProd;


			// nettoyage du fichier sql
			$file1 = pathinfo($ficTest, PATHINFO_FILENAME);
			$file2 = pathinfo($repWork, PATHINFO_FILENAME);
			$fichierProd = basename($file6);
			if (!empty($conf->global->EASYA_VERSION)) {
				$changeFileE = shell_exec("sed -i 's/DEFINER=`doliprod`/DEFINER=`dolitest`/g' $fichierProd");
				if (!empty($changeFileE)) {
					setEventMessages($langs->trans("ErrorNettoyageBDD"), null, 'errors');
				}
				else {
					setEventMessages($langs->trans("OkNettoyageBDD"), null, 'mesgs');
				}
			} else {
				$changeFile = shell_exec("sed -i 's/$nomBDDProd/$file1/g' $fichierProd");
				if (!empty($changeFile)) {
					setEventMessages($langs->trans("ErrorNettoyageBDD"), null, 'errors');
				}
				else {
					setEventMessages($langs->trans("OkNettoyageBDD"), null, 'mesgs');
				}
			}

			// TODO a remplacer par une fonction specifique
			$import = shell_exec('MYSQL_PWD="' . $dolibarr_main_db_pass . '" mysql -u ' . $dolibarr_main_db_user . ' ' . $dolibarr_main_db_name . ' < ' . $repWork2);
			if (!empty($import)) {
				setEventMessages($langs->trans("ErrorImportBDD"), null, 'errors');
				exit();
			}


			// desactivation / reactivation module oblyon
			if ($conf->global->MAIN_MODULE_OBLYON) {
				$oblyon = $prod . '/htdocs/theme/oblyon/';
				$oblyonTest = $dolibarr_main_document_root . '/theme/oblyon/';
				dolCopyDir($oblyon, $oblyonTest, 0, 1,	0, 0);

				$moduleClassName = 'modOblyon';
				$result = unActivateModule($moduleClassName);
				dolibarr_set_const($db, "MAIN_IHM_PARAMS_REV", (int) $conf->global->MAIN_IHM_PARAMS_REV + 1, 'chaine', 0, '', $conf->entity);
				if ($result) {
					setEventMessages($langs->trans("ErreurDesactivationOblyon"), null, 'errors');
				} else {
					$conf->global->MAIN_MODULE_OBLYON = 0;
					$resarray = activateModule($moduleClassName);
					dolibarr_set_const($db, "MAIN_IHM_PARAMS_REV", (int) $conf->global->MAIN_IHM_PARAMS_REV + 1, 'chaine', 0, '', $conf->entity);
					if (!empty($resarray['errors'])) {
						setEventMessages($langs->trans("ErreurActivationOblyon"), null, 'errors');
					} else {
						if ($resarray['nbperms'] > 0) {
							$tmpsql = "SELECT COUNT(rowid) as nb FROM " . MAIN_DB_PREFIX . "user WHERE admin <> 1";
							$resqltmp = $db->query($tmpsql);
							if ($resqltmp) {
								$obj = $db->fetch_object($resqltmp);
								if (
									$obj && $obj->nb > 1
								) {
									$msg = $langs->trans('ModuleEnabledAdminMustCheckRights');
									setEventMessages($msg, null);
								}
							} else {
								setEventMessages($langs->trans("ErreurOblyon"), null, 'errors');
							}
						}
					}
					$conf->global->MAIN_MODULE_OBLYON = 1;
				}
			}

			// enregistrement des modules du dictionnaire
			foreach ($moduleList as $module) {
				$sql = "INSERT INTO " . MAIN_DB_PREFIX . "dolireplic_mod_desact (module_name, label, active) values('" . $module->module_name . "', '" .		$module->label . "', '1')";
				$result = $db->query($sql);
				if ($result) {
					setEventMessages($langs->transnoentities("ErrorDicModuleDesact"), null, 'errors');
				}
			}

			// desactivation des modules (demat et suivi bancaire) indiques dans le dictionnaire
			// modDemat4Dolibarr modBanking4Dolibarr
			foreach ($moduleList as $module) {
				$result = unActivateModule($module->module_name);
				dolibarr_set_const($db, "MAIN_IHM_PARAMS_REV", (int) $conf->global->MAIN_IHM_PARAMS_REV + 1, 'chaine', 0, '', $conf->entity);
				if ($result) {
					setEventMessages($langs->trans("ErreurDesactivationModules"), null, 'errors');
				}
			}

			// desactivation dolireplic
			$moduleClassName = 'modDoliReplic';
			$result = unActivateModule($moduleClassName);
			dolibarr_set_const($db, "MAIN_IHM_PARAMS_REV", (int) $conf->global->MAIN_IHM_PARAMS_REV + 1, 'chaine', 0, '', $conf->entity);
			if ($result) {
				setEventMessages($langs->trans("ErreurDesactivationDoli"), null, 'errors');
			}

			// enregistrement des options dans la bdd
			try {
				// modification des couleurs pour oblyon
				if (!empty($conf->global->MAIN_MODULE_OBLYON)) {

					$sql = "UPDATE " . MAIN_DB_PREFIX . "const set value='#ff5733' where name='OBLYON_COLOR_TOPMENU_BCKGRD'";
					$result = $db->query($sql);
					if (!$result) {
						setEventMessages($langs->transnoentities("ErrorBddColorOblyonTop"), null, 'errors');
					}
					else {
						setEventMessages($langs->transnoentities("BddColorOblyonTop"), null, 'mesgs');
					}

					$sql = "UPDATE " . MAIN_DB_PREFIX . "const set value='#ffb833' where name='OBLYON_COLOR_LEFTMENU_BCKGRD'";
					$result = $db->query($sql);
					if (!$result) {
						setEventMessages($langs->transnoentities("ErrorBddColorOblyonLeft"), null, 'errors');
					}
					else {
						setEventMessages($langs->transnoentities("BddColorOblyonLeft"), null, 'mesgs');
					}
				}

				// changement couleur theme
				if (isset($conf->global->THEME_ELDY_TOPMENU_BACK1)) {
					$color = dolibarr_set_const($db, 'THEME_ELDY_TOPMENU_BACK1', '#de971c', 'chaine', 0, '', $conf->entity);
					if (!$color) {
						setEventMessages($langs->transnoentities("ErrorBddColorEldy"), null, 'errors');
					}
					else {
						setEventMessages($langs->transnoentities("BddColorEldy"), null, 'mesgs');
					}
				}


				// modification du niveau de log si Easya
				if (!empty($conf->global->EASYA_VERSION)) {
					$syslogvalue = '["mod_syslog_file","mod_syslog_syslog"]';
					$sql = "UPDATE " . MAIN_DB_PREFIX . "const set value='" . $syslogvalue . "' where name='SYSLOG_HANDLERS'";
					$result = $db->query($sql);
					if (!$result) {
						setEventMessages($langs->transnoentities("ErrorSyslogHandlers"), null, 'errors');
					}
					$sql = "UPDATE " . MAIN_DB_PREFIX . "const set value='LOG_LOCAL1' where name='SYSLOG_FACILITY'";
					$result = $db->query($sql);
					if (!$result) {
						setEventMessages($langs->transnoentities("ErrorSyslogFacility"), null, 'errors');
					}
					else {
						setEventMessages($langs->transnoentities("SyslogFacility"), null, 'errors');
					}
				}

				$option1 = dolibarr_set_const($db, 'DOLIREPLIC_OPT_COMP', $optionCOMP, $type = 'chaine', $visible = 1, $note = '', $entity = 0);

				$option2 = dolibarr_set_const($db, 'DOLIREPLIC_OPT_DOC', $optionDOC, $type = 'chaine', $visible = 1, $note = '', $entity = 0);

				$option3 = dolibarr_set_const($db, 'DOLIREPLIC_OPT_CUST', $optionCUST, $type = 'chaine', $visible = 1, $note = '', $entity = 0);

				$option4 = dolibarr_set_const($db, 'DOLIREPLIC_OPT_LOGO', $optionLOGO, $type = 'chaine', $visible = 1, $note = 'remplacement du logo', $entity = 0);

				$prod2 = '' . $prod . '';
				$option5 = dolibarr_set_const($db, 'DOLIREPLIC_PROD', $prod2, $type = 'chaine', $visible = 1, $note = 'chemin du dolibarr a repliquer', $entity = 0);

				// remplacement des mails
				$option6 = dolibarr_set_const($db, 'DOLIREPLIC_REPLACE_MAIL', $optionMAIL, $type = 'chaine', $visible = 1, $note = 'mail de remplacement', $entity = 0);

				$chemDocProd2 = '' . $chemDocProd . '';
				$option7 = dolibarr_set_const($db, 'DOLIREPLIC_PROD_DOC', $chemDocProd2, $type = 'chaine', $visible = 1, $note = 'chemin des documents de l instance de production', $entity = 0);

				// desactivation des mails
				$option8 = dolibarr_set_const($db, 'MAIN_DISABLE_ALL_MAILS', 1, $type = 'chaine', $visible = 1, $note = 'desactvation des mails', $entity = 0);
				if (!$option8) {
					setEventMessages($langs->transnoentities("ErrorDesactMails"), null, 'errors');
				} else {
					setEventMessages($langs->transnoentities("DesactMails"), null, 'mesgs');
				}

				// chemain des backups
				$option9 = dolibarr_set_const($db, 'DOLIREPLIC_BACKUP_DIR', $bddProd, $type = 'chaine', $visible = 1, $note = 'Chemin des backups', $entity = 0);

				// nom des backups
				$option10 = dolibarr_set_const($db, 'DOLIREPLIC_BACKUP_NAME', $backupName, $type = 'chaine', $visible = 1, $note = 'Nom du fichier de backup', $entity = 0);

				// nom bdd de prod
				$option10 = dolibarr_set_const($db, 'DOLIREPLIC_BDD_NAME_PROD', $nomBDDProd, $type = 'chaine', $visible = 1, $note = 'Nom de la bdd de prod', $entity = 0);


				// desactivation du module de notifications
				$sql = "UPDATE " . MAIN_DB_PREFIX . "const set value='0' where name='MAIN_MODULE_NOTIFICATION'";
				$result = $db->query($sql);
				if (!$result) {
					setEventMessages($langs->transnoentities("DesactNotif"), null, 'errors');
				} else {
					setEventMessages($langs->transnoentities("DesactNotif"), null, 'mesgs');
				}
			} catch (Exception $e) {
				setEventMessages("Erreur de connexion : " . $e->getMessage(), null, 'errors');
			}

			// option : replication des documents
			if ($conf->global->DOLIREPLIC_OPT_DOC == 1) {
				$docuTest = $dolibarr_main_data_root . '/';
				$docuProd = $conf->global->DOLIREPLIC_PROD_DOC . '/';
				dolCopyDir($docuProd, $docuTest, 0, 1, 0, 0);
				$reprem = $docuProd . 'admin/dolireplic';
				dol_delete_dir_recursive($reprem);
				setEventMessages($langs->transnoentities("OptionDoc"), null, 'mesgs');
			}

			// option : replication du dossier mycompany
			if ($conf->global->DOLIREPLIC_OPT_COMP == 1) {
				$myCompTest = $dolibarr_main_data_root . '/mycompany/';
				$myCompProd = $conf->global->DOLIREPLIC_PROD_DOC  . '/mycompany/';
				dolCopyDir($myCompProd, $myCompTest, 0, 1, 0, 0);
				setEventMessages($langs->transnoentities("OptionMycomp"), null, 'mesgs');
			}

			// option : replication du dossier custom
			if ($conf->global->DOLIREPLIC_OPT_CUST == 1) {
				$repCustProd = $prod . '/htdocs/custom/';
				$repCustTest = $dolibarr_main_document_root_alt . '/';

				foreach (glob($repCustTest . '*', GLOB_ONLYDIR) as $dir) {
						if(basename($dir) != 'dolireplic') {
						$suprCust = dol_delete_dir_recursive($dir);
					}
				}

				$result = dolCopyDir($repCustProd, $repCustTest, 0, 1, 0, 0);
				if (!$result) {
					setEventMessages($langs->transnoentities("ErreurOptionCustom"), null, 'errors');
				} else {
					setEventMessages($langs->transnoentities("OptionCustom"), null, 'mesgs');
				}
			}

			// option : remplacement des mails
			$optionMAIL = $conf->global->DOLIREPLIC_REPLACE_MAIL;
			if (!empty($conf->global->DOLIREPLIC_REPLACE_MAIL)) {

				$sql = "UPDATE " . MAIN_DB_PREFIX . "societe SET email = '" . $optionMAIL . "' WHERE email IS NOT NULL;";
				$result = $db->query($sql);
				if (!$result) {
					setEventMessages($langs->transnoentities("ErreurOptionMailSoc"), null, 'errors');
				} else {
					setEventMessages($langs->transnoentities("RemplacementMailSoc"), null, 'mesgs');
				}

				$sql = "UPDATE " . MAIN_DB_PREFIX . "socpeople SET email = '" . $optionMAIL . "' WHERE email IS NOT NULL;";
				$result = $db->query($sql);
				if (!$result) {
					setEventMessages($langs->transnoentities("ErreurOptionMailSocp"), null, 'errors');
				} else {
					setEventMessages($langs->transnoentities("RemplacementMailSocp"), null, 'mesgs');
				}

				$sql = "UPDATE " . MAIN_DB_PREFIX . "adherent SET email = '" . $optionMAIL . "' WHERE email IS NOT NULL;";
				$result = $db->query($sql);
				if (!$result) {
					setEventMessages($langs->transnoentities("ErreurOptionMailAdh"), null, 'errors');
				} else {
					setEventMessages($langs->transnoentities("RemplacementMailAdh"), null, 'mesgs');
				}

				$sql = "UPDATE " . MAIN_DB_PREFIX . "user SET email = '" . $optionMAIL . "' WHERE email IS NOT NULL;";
				$result = $db->query($sql);
				if (!$result) {
					setEventMessages($langs->transnoentities("ErreurOptionMailUser"), null, 'errors');
				} else {
					setEventMessages($langs->transnoentities("RemplacementMailUser"), null, 'mesgs');
				}
			}

			// option : copie du logo de test dans les differents formats
			if ($conf->global->DOLIREPLIC_OPT_LOGO == 1) {
				$chem = $dolibarr_main_document_root_alt . '/dolireplic/img/logo.png';
				$logoDest = $dolibarr_main_data_root . '/mycompany/logos/logo.png';
				$logoDest2 = $dolibarr_main_data_root . '/mycompany/logos';
				$logoDest3 = $dolibarr_main_data_root . '/mycompany/logos/thumbs';

				if (!file_exists($logoDest2)) {
					dol_mkdir($logoDest2);
				}

				if (!file_exists($logoDest3)) {
					dol_mkdir($logoDest3);
				}

				$dir = 'cp ' . $chem . ' ' . $logoDest;
				$result = $utils->executeCLI($dir, $outputfile);

				$dir = 'cp ' . $chem . ' ' . $logoDest3;
				$result = $utils->executeCLI($dir, $outputfile);


				$sql = "UPDATE " . MAIN_DB_PREFIX . "const SET value = 'logo.png' WHERE name = 'MAIN_INFO_SOCIETE_LOGO' OR name = 'MAIN_INFO_SOCIETE_LOGO_SMALL' OR name = 'MAIN_INFO_SOCIETE_LOGO_MINI' OR name = 'MAIN_INFO_SOCIETE_LOGO_SQUARRED' OR name = 'MAIN_INFO_SOCIETE_LOGO_SQUARRED_SMALL' OR name = 'MAIN_INFO_SOCIETE_LOGO_SQUARRED_MINI'";
				$result = $db->query($sql);
				if (!$result) {
					setEventMessages($langs->transnoentities("ErrorDLogo"), null, 'errors');
				} else {
					setEventMessages($langs->transnoentities("CopieLogo"), null, 'mesgs');
				}
			}
			setEventMessages($langs->transnoentities("ReplicDone"), null, 'mesgs');

			// recharger la page apres desactivation / reactivation du module
			// header('Location: ' . $_SERVER['PHP_SELF']);
			$locationTarget = DOL_URL_ROOT . '/index.php';
			header('Location: ' . $locationTarget);
			exit();
		}
	}
}



/*
 * View
 */

$form = new Form($db);
$formfile = new FormFile($db);

llxHeader("", $langs->transnoentities("DoliReplicAreaReplic"));

print load_fiche_titre($langs->transnoentities("DoliReplicAreaReplic"), '', 'objreplic@dolireplic');

if (!empty($conf->dolireplic->enabled) && ($user->rights->dolireplic->dolireplic->read) && ($user->rights->dolireplic->dolireplic->write) && ($user->rights->dolireplic->dolireplic->delete)) {


	$sortfield = 'date';
	$sortorder = 'asc';

	// liste des fichiers sauvegardes
	$back2 = $conf->admin->dir_output . '/dolireplic/backup/';

	print '<div class="twocolumns">';
	print '<div class="titre inline-block centpercent">';
	print '<span class="fa fa-server"></span> Réplication et fichiers<br><br>';
	print '</div>';

	print '<div class="firstcolumn fichehalfleft boxhalfleft" id="boxhalfleft">';
	print '<div class="titre">';
	print 'Fichiers de sauvegarde';
	print '</div>';
	print '<br>';
	print '<div id="backupfileright" class="border centpercent" style="height:250px; overflow: auto;">';
	$filearray = dol_dir_list($back2, 'files', 0, '', '', $sortfield, (strtolower($sortorder) == 'asc' ? SORT_ASC : SORT_DESC), 1);
	$result = $formfile->list_of_documents($filearray, null, 'systemtools', '', 1, '/dolireplic/backup/', 1, 0, $langs->transnoentities("NoBackupFileAvailable"), 0, 'none');
	print '</div>';
	print '</div>';


	print '<div class="secondcolumn fichehalfright boxhalfright" id="boxhalfright">';
	print '<div class="titre inline-block centpercent">';
	print 'Réplication';
	print '</div>';
	print '<br><br>';
	print '<div class="essai" style="font-size: 16px; line-height: 1.5;">';
	print '<strong>Réplication d\'une instance de production Dolibarr sur une instance de test.</strong><br>';
	print 'Par défaut, copie de la base de données, désactivation des mails et des notifications.<br>';
	print 'Diverses options sont disponibles, veuillez vérifiez les options du module avant de lancer la réplication.<br>';
	print 'Vérifiez la taille du dossier documents, affichée sur la page de configuration du module.<br>';
	print 'Il est conseillé de vider le cache une fois terminée la réplication';
	print '<br>';
	print '</div>';
	print '<div class="centpercent">';
	print '<br>';

	print '</div>';

	print '<br><br>';

	print '<a class="butAction" href="' . $_SERVER["PHP_SELF"] . '?socid=' . (!empty($socid) ? '?socid=' . $socid : '') . '&action=startReplic&token=' . newToken() . '">' . $langs->transnoentities("Replication") . '</a>' . "\n";

	print '</div>';

	print '</div>';
} else {
	accessforbidden();
}

// End of page
llxFooter();
$db->close();
