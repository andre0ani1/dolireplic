<?php
/* Copyright (C) 2020      Open-DSI             <support@open-dsi.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


class Dolireplic
{
 /**
     * @var string Error
     */
    public $error = '';
    /**
     * @var array Errors
     */
    public $errors = array();

    /**
     * Constructor
     *
     * @param        DoliDB $_db Database handler
     */
    public function __construct()
    {

    }


    	/**
	 * Execute a CLI command.
	 *
	 * @param 	string	$command			Command line to execute.
	 * 										Warning: The command line is sanitize so can't contains any redirection char '>'. Use param $redirectionfile if you need it.
	 * @param 	string	$outputfile			A path for an output file (used only when method is 2). For example: $conf->admin->dir_temp.'/out.tmp';
     * @param 	string	$inputfile			A path for an intput file
	 * @param	int		$execmethod			0=Use default method (that is 1 by default), 1=Use the PHP 'exec', 2=Use the 'popen' method
	 * @param	string	$redirectionfile	If defined, a redirection of output to this files is added.
	 * @param	int		$noescapecommand	1=Do not escape command. Warning: Using this parameter need you alreay sanitized the command. if not, it will lead to security vulnerability.
	 * 										This parameter is provided for backward compatibility with external modules. Always use 0 in core.
	 * @return	array						array('result'=>...,'output'=>...,'error'=>...). result = 0 means OK.
	 */
	public static function doliCom($command, /* $outputfile, */ $inputfile, $execmethod = 1, /* $redirectionfile = null,*/ $noescapecommand = 1)
	{
		global $conf, $langs;

		$result = 0;
		$output = '';
		$error = '';

		if (empty($noescapecommand)) {
			$command = escapeshellcmd($command);
		}
		if ($inputfile) {
			$command .= " < " . $inputfile;
		}
		$command .= " 2>&1";

		if (!empty($conf->global->MAIN_EXEC_USE_POPEN)) {
			$execmethod = $conf->global->MAIN_EXEC_USE_POPEN;
		}
		/* if (empty($execmethod)) {
			$execmethod = 1;
		} */
		//$execmethod=1;

		dol_syslog("Utils::doliCom execmethod=".$execmethod." system:".$command, LOG_DEBUG);
		$output_arr = array();

		// if ($execmethod == 1) {
			$retval = null;

			$handle = fopen($inputfile, 'w+b');
			if ($handle) {
				$handlein = popen($inputfile, 'r');
				while (!feof($handlein)) {
					$read = fgets($handlein);
					// fwrite($handle, $read);
					$output_arr[] = $read;
				}
				pclose($handlein);
			}

			exec($command, $output_arr, $retval);
			$result = $retval;
			if ($retval != 0) {
				$langs->load("errors");
				dol_syslog("Utils::doliCom retval after exec=".$retval, LOG_ERR);
				$error = 'Error '.$retval;
			}
		// }
		/* if ($execmethod == 2) {	// With this method, there is no way to get the return code, only output
			$handle = fopen($outputfile, 'w+b');
            $handle1 = fopen($inputfile, 'w+b');
			if ($handle && $handle1) {
				dol_syslog("Utils::executeCLI run command ".$command);
				$handlein = popen($command, 'r');
				while (!feof($handlein)) {
					$read = fgets($handlein);
					fwrite($handle, $read);
					$output_arr[] = $read;
				}
				pclose($handlein);
				fclose($handle);
			}
			if (!empty($conf->global->MAIN_UMASK)) {
				@chmod($outputfile, octdec($conf->global->MAIN_UMASK));
			}
		} */

		// Update with result
		if (is_array($output_arr) && count($output_arr) > 0) {
			foreach ($output_arr as $val) {
				$output .= $val.($execmethod == 2 ? '' : "\n");
			}
		}

		dol_syslog("Utils::doliCom result=".$result." output=".$output." error=".$error, LOG_DEBUG);

		return array('result'=>$result, 'output'=>$output, 'error'=>$error);
	}


}